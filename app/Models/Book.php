<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use HasFactory , SoftDeletes;  //first add trate , then we add a new col in creatte table migration >>Deleted At 
    //we have five process > Trashed , withtrash , onlytrash , forcedelete , restore
    //السوفت ديليت تحويل النل الى قيمة
    public function getVisibilityStatusAttribute()
    {
        return $this->is_visible ? 'visible' : 'Hidden';
    }

    
    public function getlanguageNameAttribute(){
        return $this->language == 'en' ? 'English' : 'Arabic';
    }

    public function category(){

        return $this->belongsTo(Category::class,'category_id','id');

    }
}
