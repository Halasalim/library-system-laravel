<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    //add attribute in show stage only 
    //add object to the table that not appear to the user 

    protected $appends =['visibility_status']; 

    //get attribute without appends does not add the new attribute to the model 
    //how model deal with boolean ? visible is array in the laravel system
    //so when we use it we call something in the system >> rreserved keyword
    //we should change the name of the colum in the table
    public function getVisibilityStatusAttribute(){
        return $this->is_visible ? 'visible' : 'Hidden' ;
    }


    //Relation 
    public function books(){
        return $this->hasMany(Book::class,'category_id','id');
    }
}
