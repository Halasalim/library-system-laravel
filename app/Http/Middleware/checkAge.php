<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class checkAge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next ,$age)
    {
        //...$age > list of datatypes 
            //Befor 
            //    $age = 18;
           //   $age = 22;
               if($age > 20){
                return $next($request);
               }else{
                   abort(403,'Your age is less than 20');
               }
        
               //after 
            //    $response =$next($request);
            //    if($age >20){
            //        dd($response);
            //        if($response->active){
            //            //true 
            //            return $response;
            //        }else{
            //         abort(403,'your account is not active');
            //        }
            //    }else{
            //     abort(403,'Your age is less than 20');
            //    }



        //BEFOR -MIDDLEWARE - (checks ) >> success ==next request >> means continue 
        //Falied >> redirect or reject
        //code or checks that i need (pass or faile)
       // return $next($request);



        //After -middleware // carry out the operation then check 
        // $response = $next($request);
        // return $response;


    }
}
