<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //by defualt withoutTrashed 
        //  $books = Book::with('category')->get(); // this will retrun all  books that the deleted at value is NUll
        //  $books = Book::with('category')->onlyTrashed()->get(); //just deleted books
        // $books = Book::with('category')->withoutTrashed()->get();
        $books = Book::with('category')->withTrashed()->get(); //return also deleted books with non deleted
        // بدنا نرجع الداتا 
        //if web > view   mobile api > json 
        //بدنا نفحص ال expect json or work on guard and this is the best way 
        //type of guard > api json  , web view 
        if (auth('api')->check()) {
            return response()->json(['data' => $books]);
        } else {
            //view 
            return response()->view('cms.Books.index', ['books' => $books]);
        }
        //we have anthor method to check the api 
        //    if(request()->expectsjson()){
        //   js , node js , vue js , api 
        // In header we add Key Accept  - value appliaction/json 
        //    }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::where('is_visible', true)->get();
        return  response()->view('cms.Books.create', ['categories' => $categories]);

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:3|max:100',
            'year' => 'required|numeric|digits:4',
            'language' => 'required|string|in:en,ar',
            'quantity' => 'required|integer|min:1',
            // 'visible' => 'required|boolean',
            'visible' => 'required|in:true,false',
            'image' => 'required|image|mimes:jpg,png|max:2048',
            'category_id' => 'required|integer|exists:categories,id'
        ]);

        if (!$validator->fails()) {
            $book = new Book();
            $book->name = $request->get('name');
            $book->year = $request->get('year');
            $book->language = $request->get('language');
            $book->quantity = $request->get('quantity');
            $book->category_id = $request->get('category_id');
            // Category::findOrFail($request->get('category_id'))->books()->save($book);

            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $imageName = time() . '_' . $book->name . '.' . $image->getClientOriginalExtension();
                $request->file('image')->storePubliclyAs('books', $imageName, ['disk' => 'public']);
                $book->image = $imageName;
            }
            $isSaved = $book->save();

            return response()->json([
                'message' => $isSaved ? 'Saved successfully' : 'Failed to create new book!'
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
        $categories = Category::where('is_visible', true)->get();
        return response()->view('cms.Books.edit', ['book' => $book, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        //
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:3|max:100',
            'year' => 'required|numeric|digits:4',
            'language' => 'required|string|in:en,ar',
            'quantity' => 'required|integer|min:1',
            // 'visible' => 'required|boolean',
            'visible' => 'required|in:true,false',
            'image' => 'nullable|image|mimes:jpg,png|max:2048',
            'category_id' => 'required|integer|exists:categories,id'
        ]);

        if (!$validator->fails()) {
            $book->name = $request->get('name');
            $book->year = $request->get('year');
            $book->language = $request->get('language');
            $book->quantity = $request->get('quantity');
            $book->category_id = $request->get('category_id');
            // Category::findOrFail($request->get('category_id'))->books()->save($book);

            if ($request->hasFile('image')) {
                // Storage::delete("/books/$book->image");
                Storage::disk('public')->delete("books/$book->image"); //عشان يحذف الصور القديمة
                //delete path
                $image = $request->file('image');
                $imageName = time() . '_' . $book->name . '.' . $image->getClientOriginalExtension();
                $request->file('image')->storePubliclyAs('books', $imageName, ['disk' => 'public']);
                $book->image = $imageName;
            }
            $isSaved = $book->save();

            return response()->json([
                'message' => $isSaved ? 'Saved successfully' : 'Failed to create new book!'
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        //اذا بدي احذف حذف تام = forcdelete
        //softdelete is event gose on database to convert softdelete to deleted At col

        //$isDeleted = $book->delete();
        // return response()->json(
        //     [
        //         'icon' => $isDeleted ? 'success' : ' danger',
        //         'title' => $isDeleted ? 'Deleded successfully' : 'Failed to delete'
        //     ],
        //     $isDeleted ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST
        // );
        $book = Book::withTrashed()->findOrFail($id);
        $message = '';
        if ($book->trashed()) {
            $status = $book->restore();
            $message = 'Book restored successfuly';
        } else {
            //    $book = $book->delete();
            //         $message = 'Book Deleted successfuly';
            $isDeleted = $book->delete();
            return response()->json(
                [
                    'icon' => $isDeleted ? 'success' : ' danger',
                    'title' => $isDeleted ? 'Deleded successfully' : 'Failed to delete'
                ],
                $isDeleted ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST
            );
        }
        if ($status) {
            return response()->json(['title' =>  $message, 'icon' => 'success']); //key>>string""  value>>int object double boolean array of any type or array of json  //begin with objects or array
        } else {
            return response()->json(['title' => 'Process failed', 'icon' => 'danger']);
        }
    }
}
