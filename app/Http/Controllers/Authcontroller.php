<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Hash;

class Authcontroller extends Controller
{
    //
      public function showlogin(Request $request , $guard){
          //we send the value of the guard attribute to login 
          return response()->view('cms.auth.login', ['guard'=>$guard]);
      }

    public function login(Request $request)
    {
        $validator = Validator($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string|min:3|max:10',
            'remember' => 'required|boolean',
            'guard' => 'required|in:admin,user|string'
        ], [
            'guard.in' => 'Please, check url'
        ]);

        $credentials = ['email' => $request->get('email'), 'password' => $request->get('password')];
        if (!$validator->fails()) {
            if (Auth::guard($request->get('guard'))->attempt($credentials, $request->get('remember'))) {
                return response()->json(['message' => 'Logged in successfully'], Response::HTTP_OK);
            } else {
                return response()->json(['message' => 'Error credentials, please try again'], Response::HTTP_BAD_REQUEST);
            }
        } else {
            return response()->json(['message' => $validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function logout(Request $request){
        //one of two > admin or user > check 
        // admin > guard
        //user > guard 
           $guard =auth('admin')->check() ? 'admin' :'user';
           Auth::guard($guard)->logout();
           $request->session()->invalidate();
           return redirect()->route('cms.login',$guard);
    }


    //change Password functions 
    //we have two functions : 1- show the change interface password
    //2- update or change 

    public function showChangePassword(Request $request)
    {
           return response()->view('cms.auth.changePassword');
    }

    public function changePassword(Request $request)

    {
        $validator =Validator($request->all(),[
             'password'=>'required|password:admin',
              'new_password' => 'required|string|confirmed',
              //take this key and search for new password confrimation by default 
        ]);

        if(!$validator->fails()){
            $guard = auth('admin')->check() ? 'admin' : 'user';
            $user =auth($guard)->user();
            $user->password=Hash::make($request->get('new_password'));
            $isSaved = $user->save();
            return response()->json(['message' => $isSaved ? 'Password changed successfully' : 'Failed to change password '], Response::HTTP_OK);


        }else{
            return response()->json(['message' => $validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);
        }

    }


}
