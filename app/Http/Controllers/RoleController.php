<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Symfony\Component\HttpFoundation\Request as HttpFoundationRequest;
use Symfony\Component\HttpFoundation\Response;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     //Role created based on spesifc guard and the user take role that have permission from the same guard
     //
    public function index()
    {
        //
        // $roles = Role::all();      
        //to get number of permissions for the role 
        $roles = Role::withCount('permissions')->get();
        return response()->view('cms.roles.index',['roles'=>$roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return response()->view('cms.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator($request->all(),[
              'name'=>'required|string|min:3|max:40',
              'guard'=> 'required|string|in:user,admin',
        ]);
        if(!$validator->fails()){
                $role = new Role();
                $role->name =$request->get('name');
                $role->guard_name =$request->get('name');
                $isSaved =$role->save();
                 return response()->json(['message'=> $isSaved ? 'Saved successfully': 'Failed to save'],$isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        }else{
                return response()->json(['message'=> $validator->getMessageBag()->first(),Response::HTTP_BAD_REQUEST]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
        // $isDeleted = $role->delete();
        //  return response()->json(['message'=>$isDeleted ? 'Deleted successfully' : 'Failed to delete role'], 
        //  $isDeleted ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST);

        $deleted = $role->delete();
        if ($deleted) {
            return response()->json(['title' => 'Deleted successfully', 'icon' => 'success']); //key>>string""  value>>int object double boolean array of any type or array of json  //begin with objects or array
        } else {
            return response()->json(['title' => 'Deleted failed', 'icon' => 'warining']);
        }
    }
}
