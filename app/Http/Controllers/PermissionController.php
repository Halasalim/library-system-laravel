<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;
use Symfony\Component\HttpFoundation\Response;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $permissions = Permission::all();
        return response()->view('cms.permissions.index',['permissions'=> $permissions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
 
        return response()->view('cms.permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //  dd($request);
        $validator = Validator($request->all(),[
              'name'=>'required|string|min:3|max:40',
               'guard'=>'required|string|in:admin,user',
        ]);
        if(!$validator->fails()){
               
            $permissions = new Permission();
            $permissions->name=$request->get('name');
            $permissions->guard_name=$request->get('guard');
            $isSaved = $permissions->save();
              return response()->json(['message' => $isSaved ? " Permission Created successfully " : " Failed to create Permissions"],
                $isSaved ? Response::HTTP_CREATED: Response::HTTP_BAD_REQUEST);
        }else{
            return response()->json(['message' =>$validator->getMessageBag()->first()],Response::HTTP_BAD_REQUEST);

        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        //
        $isDeleted =  $permission->delete();
         return response()->json([
             'icon'=> $isDeleted ? 'success' : 'warining' ,
             'title' =>$isDeleted ? 'Deleted successfully ' : 'Failed to delete permission'
         ] , $isDeleted ? Response ::HTTP_OK : Response ::HTTP_BAD_REQUEST);
    }
}
