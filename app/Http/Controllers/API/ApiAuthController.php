<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use PhpParser\Node\Stmt\TryCatch;
use Symfony\Component\HttpFoundation\Response;

class ApiAuthController extends Controller
{
    //Here we use Passport library
    //انشاء كلاس مسؤول عن عملية تسجيل المستخدمين في ال api
    //In API we have to method tp login:-
    //1-Personal Access Token عام ينشأ من غير تخصيص او توجييه على بروفيدر معين)(Provider = Null)
    //2-personal grant client token مخصص user or admin
    //ينشأ مع بيئة العمل لارافيل عند تنزيل مكتبة باسبورت وتنفيذ امر php artisan passport install
    // نوعين من الكلاينتس وبقدر ازيد (Php artisan passport:client -- password) بمجرد الانشاء رح يطلب البروفيدر و الاسم 
    //عشان اخلي البروفيدر يتصل مع جارد ويكون بدعم الاي بي اي فلازم يكون الجاردتبع البروفيدر باسبورت 
    //provider DB table of model eloquent and should have trate > HasApiTokens اضافت على المودل خاضية  اضافة توكين بس لازمها اسم 
    //يستخدم الاسم لعملية الادارة الخاصة في توضيح التوكين لمين انشاءها عشان ما عندي بروفيدر ولاي يوزر ولاي جارد 

    //first login --  MultiUsers at the same time 
    // public function login(Request $request)
    // {


    //     $validator = Validator($request->all(), [
    //         'email' => 'required|email|string|exists:users,email',
    //         'password' => 'required|string',
    //         //role >password we can't use it untile we are authenticated
    //     ]);
    //     if (!$validator->fails()) {
    //         $user = User::where('email', $request->get('email'))->first();
    //         if (Hash::check($request->get('password'), $user->password)) {
    //             //Create -TOKEN
    //             if(!$this->checkloginStatus($user->id)){
    //                 $token = $user->createToken('user-Token');
    //                 $user->setAttribute('token', $token->accessToken);
    //                 return  response()->json([

    //                     'status' => true,
    //                     'message' => 'logged in successfully',
    //                     'data' => $user,
    //                     //'token'=>$token,
    //                 ]);
    //             }else{
    //                  return  response()->json([

    //                     'status' => false,
    //                     'message' => 'unable to login from two devices as the same time .' ]);

    //             }

    //         } else {
    //             return response()->json(['message' => 'Login failed ,wrong credentails'], Response::HTTP_BAD_REQUEST);
    //         }
    //     } else {
    //         return response()->json(['message' => $validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);
    //     }
    // }



    //Login and revoked active logins 
    // public function login(Request $request)
    // {

    //     $validator = Validator($request->all(), [
    //         'email' => 'required|email|string|exists:users,email',
    //         'password' => 'required|string',
    //         //role >password we can't use it untile we are authenticated
    //     ]);
    //     if (!$validator->fails()) {
    //         $user = User::where('email', $request->get('email'))->first();
    //         if (Hash::check($request->get('password'), $user->password)) {
    //             //REVOKED ACTIVE LOGINS 
    //             $this->revokeActiveToken($user->id);
    //             //Create -TOKEN
    //                 $token = $user->createToken('user-Token');
    //                 $user->setAttribute('token', $token->accessToken);
    //                 return  response()->json([

    //                     'status' => true,
    //                     'message' => 'logged in successfully',
    //                     'data' => $user,
    //                     //'token'=>$token,
    //                 ]);
    //             } 
    //             else {
    //             return response()->json(['message' => 'Login failed ,wrong credentails'], Response::HTTP_BAD_REQUEST);
    //         } 
    //     } else {
    //         return response()->json(['message' => $validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);
    //     }
    // }


    //The seconed way -  Laravel Password Grant Client dealing with apis 
    public function login(Request $request)
    {

        $validator = Validator($request->all(), [
            'email' => 'required|email|string|exists:users,email',
            'password' => 'required|string',
            //role >password we can't use it untile we are authenticated
        ]);
        if (!$validator->fails()) {
           
                //creat Passowrd grant client token - Multi Auth  - use provider
                return $this->generatePasswordGrantToken($request);
                //problem will happen here bacuse we can't use the same port  in localhost but in web its okay 
                //php artisan serve --port=8001 open new port

        } else {
            return response()->json(['message' => $validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);
        }
    }



    public function generatePasswordGrantToken(Request $request)
    {

        try {
            //code...
            //we have to get token 
            //here we know the provider based on secret and id , if their is information (email, password ) as the same 
            //==token 
            $response = Http::asForm()->post('http://127.0.0.1:8001/oauth/token', [
                'grant_type' => 'password',
                'client_id' => '2',
                'client_secret' => 'OxP83wU06D6DNggxlO2AnbN3DsRil2L4YjKvLYTZ',
                'username' => $request->get('email'),
                'password' => $request->get('password'),
                'scope' => '*'  //all مدى عمومية التوكين على صلاحية النظام 
                // وبقدر اعمله رول و بريمشينز
                //all or this will return token type (PGC)

            ]);
            //return response()->json();
            //  return $response;// to check if thier an error 
            //   {"error":"invalid_grant","error_description":"The user credentials were incorrect.","message":"The user credentials were incorrect."}
            $user = User::where('email', $request->get('email'))->first();
            $user->setAttribute('token', $response->json()['access_token']);
            $user->setAttribute('token_type', $response->json()['token_type']);
            return  response()->json([

                'status' => true,
                'message' => 'logged in successfully',
                'data' => $user,

            ]);
        } catch (\Exception $exception) {
            //throw $th;
        // dd($response['error']);
             //invalid_grant exception error problem in email or password 
             $messag='';
             if($response['error']== 'invalid_grant'){
            // return response()->json(['message' => 'Login failed ,wrong credentails'],
            //  Response::HTTP_BAD_REQUEST);
            $messag = 'Login failed ,wrong credentails';

             } else{
                //    return response()->json(['message' => 'something went wrong ,please try again '],
                //     Response::HTTP_BAD_REQUEST);
                $messag = 'something went wrong ,please try again ';

             }   
             
             
                return response()->json(
                    [
                        'status'=>false,
                        'message' => $messag
                    
                    ],
                    Response::HTTP_BAD_REQUEST);

   
        }
      
    }

    //login seconed method if their any device is active decline login //active logins 
    public function checkloginStatus($userId)
    {
        //Note their is no model for oauth_access_tokens so we use DB
        return  DB::table('oauth_access_tokens')
            ->where('user_id', $userId)
            ->where('name', 'user-Token')
            ->where('revoked', false) //still active
            ->count();
    }

    //function to revoked all the activelogins before
    public function revokeActiveToken($userId)
    {
        DB::table('oauth_access_tokens')
            ->where('user_id', $userId)
            ->where('name', 'user-Token')
            ->update(['revoked' => true]);
    }




    //logout function
    public function logout(Request $request)
    {
        //guard = api = user = token
        $token = auth('api')->user()->token();
        //return response()->json(['token' => $token]);
        $revoked = $token->revoke();
        return response()->json(['status' => $revoked, 'message' => $revoked ? 'Logged out successfully' : 'failed to logged out'], $revoked ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST);
    }
}

// User -> HasApiToken createToken
//  /**
//      * Create a new personal access token for the user.
//      *
//      * @param  string  $name
//      * @param  array  $scopes
//      * @return \Laravel\Passport\PersonalAccessTokenResult
//      */
//  
//     public function createToken($name, array $scopes = [])
//     {
//         return Container::getInstance()->make(PersonalAccessTokenFactory::class)->make(
//             $this->getKey(), $name, $scopes
//         );
//     }
