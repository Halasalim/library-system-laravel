<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Book;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BookController extends Controller
{
    /**
     * Display a listing of the resource. api 5 function 
     * //create and edit are deleted 
     * //the user can't reach books if he is not register >> should be authenticated user >>middleware on guard api 
     * //should token sent 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $books =Book::all();
        //IF we have a huge number of books and we need just 10 books in the page 
        //we use pagenation (10)
        // $books = Book::paginate(1); //for web more details 
        $books = Book::simplePaginate(10); //for mobile  more useable 
        //create object and set inside it listed data 
        return response()->json(['status'=>true , 'message'=>'sucess' , 'data'=>$books]);
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator($request->all(), [

            'name' => 'required|string|min:3|max:100',
            'year' => 'required|numeric|digits:4',
            'language' => 'required|string|in:en,ar',
            'quantity' => 'required|integer|min:1',
            'visible' => 'required|boolean',
            'image' => 'nullabel|image|mimes:jpg,png|size:2048',
            'category_id' => 'required|integer|exists:categories,id' //

        ]);
        if (!$validator->fails()) {
            $book = new Book();
            $book->name = $request->get('name');
            $book->year = $request->get('year');
            $book->language = $request->get('language');
            $book->quantity = $request->get('quantity');
            $book->is_visible = $request->get('visible');
            $book->category_id = $request->get('category_id');


            $isSaved = $book->save();
            //  Category::findOrFail($request->get('category_id'))->books->save($book);

            return response()->json(
                ['message' => $isSaved ? 'Saved successfully' : ' Failed to save'],
                $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST
            );
        } else {
            return response()->json(
                ['message' => $validator->getMessageBag()->first()],
                Response::HTTP_BAD_REQUEST
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        //
        $validator = Validator($request->all(), [

            'name' => 'required|string|min:3|max:100',
            'year' => 'required|numeric|digits:4',
            'language' => 'required|string|in:en,ar',
            'quantity' => 'required|integer|min:1',
            'visible' => 'required|boolean',
            'image' => 'nullabel|image|mimes:jpg,png|size:2048',
            'category_id' => 'required|integer|exists:categories,id' //

        ]);
        if (!$validator->fails()) {
            $book->name = $request->get('name');
            $book->year = $request->get('year');
            $book->language = $request->get('language');
            $book->quantity = $request->get('quantity');
            $book->is_visible = $request->get('visible');
            $book->category_id = $request->get('category_id');


            $isSaved = $book->save();
            //  Category::findOrFail($request->get('category_id'))->books->save($book);

            return response()->json(
                ['message' => $isSaved ? 'Saved successfully' : ' Failed to save'],
                $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST
            );
        } else {
            return response()->json(
                ['message' => $validator->getMessageBag()->first()],
                Response::HTTP_BAD_REQUEST
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // $deleted = Book::destroy($id);
        $deleted = Book::findOrFail($id)->delete();
        return response()->json(['status'=>$deleted , 'message'=> $deleted ? 'Deleted successfully' : 'Deleted faild'],
    $deleted ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST
    
    );
    }
}
