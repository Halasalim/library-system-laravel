<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

class RolePermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator($request->all(), [
            'role_id' => 'required|integer|exists:roles,id',
            'permission_id' => 'required|integer|exists:permissions,id'

        ]);
           // هل هذه الرول تحتوي في داخلها على بريمشينز من الاي دي ؟
        if( !$validator->fails()){
            /////// علامة التعججججججججججبببببب
            $role = Role::findOrFail($request->get('role_id'));
            $permission = Permission::findOrFail($request->get('permission_id'));

            if($role->hasPermissionTo($permission)){
                //many to many reltionship between role and permission
                 $role->revokePermissionTo($permission);
                //delete this role and permissions 
                //else give insert
                return response()->json(['message' => 'Permission removed successfully'], Response::HTTP_OK);
            }else{
                //اذا كان معطى  ارفضه واذا كان غير معطى اعطيه
                $role->givePermissionTo($permission);
                return response()->json(['message' => 'Permission assigned successfully'], Response::HTTP_OK);
            }
            return response()->json(['message' => 'Permission updated successfully' ] ,Response::HTTP_OK);
        }else{
            return response()->json(['message' => $validator->getMessageBag()->first(), Response::HTTP_BAD_REQUEST]);
        }
       
    }

    
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //we have id of role and we need to get the permissions for this role 

        $role = Role::findOrFail($id);
        $rolePermissions = $role->permissions;
        //we need to get all the permissions where  it is from the same guard role name 

        $permissions = Permission::where('guard_name',$role->guard_name)->get();
        //forloop   
        //we have permission and role permissions
        foreach ($permissions as $permission){
            //here we add sth >> assigned by default false because maybe we dont have any permission for the role 
            //public 
            $permission->setAttribute('assigned', false);
           
            foreach ($rolePermissions  as  $rolePermission){
                 if($rolePermission->id == $permission->id){
                    //means that the roleper id > are the same id of any per  //change it to true 
                    $permission->setAttribute('assigned', true);
                    //اذا تطابقت الاي دي رح يعمل اسين 
                    // return;
                     break;
                 }
            }
        }
        return  response()->view('cms.roles.role-permission',['role'=>$role, 'permissions'=>$permissions]);
        //17 -- here 47:00 min 

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
