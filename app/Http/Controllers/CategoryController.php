<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    //1- resourse controller +2- model binding  + 3-model name 

    //each rout talks with func contorller >> policy function according type 
    //every function in controller >> policy function 
    //we can apply authorize resourse at once or write i at each function
    //Note : name of ability >> name of policy function
    public function __construct()
    {
        $this->authorizeResource(Category::class, 'category'); //make connect between controller and policy 
        //يعمم على الجميع بشرط يكون الكونترولر ريسورس مودل باينديغ 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // test
/*         echo "we are in index "; */
         //QUERY ELOQUNT TO RETURN DATA 
        // $data=Category::all(); //if we need where >>get
        $data = Category::withCount('books')->get(); 
         return response()->view('cms.categories.index',['categories'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
      //  $this->authorize('create', Category::class); //we should write it in each func when the attribute is binding
        return response()->view('cms.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //form button submit >> every input >> value to store method post
        //dd($request->all());
        //we have sth called page Expired >>419 (الحماية من عمليات الاستخدام غير الامنة ) 	@csrf Token 
        //we have to make validation  on data 
      //  $this->authorize('create', Category::class);
         $request->validate([
             'name'=>'required|string|min:3|max:20',
             'description'=>'nullable|string|min:3|max:50',
             'visible'=>'in:on|string'     //when we click submit there is sth say like return , but befor appear debug error 
         ],[
                 'name.required'=>'Please enter name !'

         ]);
         
        $category = new Category();
        $category->name =$request->get('name');
        $category->description =$request->get('Description');
        $category->is_visible=$request->has('visible');
        $isSaved=$category->save();
        if($isSaved){
         //   return redirect()->route('categories.index');
         session()->flash('type','success');
         session()->flash('message','category saved successfully'); //quick message read then disapeard 

            return redirect()->back();   
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
       
        //$this ->authorize('view',[$category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $category = Category::find($id);
        $this->authorize('update',[Category::find($id)]);
        // we send parameter >find or fail > model binding 
        return response()->view('cms.categories.edit',['category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
        $request-> validate([
            'name'=>'required|string|min:3|max:20',
            'description'=>'nullable|string|min:3|max:50',
            'visible'=>'in:on|string' 

        ]);
    $category->name=$request->get('name');
    $category->description=$request->get('description');
    $category->is_visible=$request->has('visible');
    $isSaved =$category->save();
    if($isSaved){
        return redirect()->route('categories.index');

    }else{
        session()->flash('message','updated failed !');
        session()->flash('type','danger');
        return redirect()->back();
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //primary key > id we didn't change it 
        $deleted= $category->delete();
        if($deleted){
             return response()->json(['title'=>'Deleted successfully','icon'=>'success']); //key>>string""  value>>int object double boolean array of any type or array of json  //begin with objects or array
        }else{
            return response()->json(['title'=>'Deleted failed','icon'=>'danger']);
        }
    //     if($deleted)
    //     return redirect()->back();
    // }
}
}
