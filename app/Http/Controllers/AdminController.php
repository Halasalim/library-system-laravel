<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return all of admins on the system exclude me 
        $admins = Admin::where('id','!=',auth('admin')->id())->get();
        return response()->view('cms.admins.index',['admins'=>$admins]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //create admin with role 

         $roles= Role::where('guard_name','admin')->get(); 
          return response()->view('cms.admins.create',['roles'=>$roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //unique:admins,email >> new role in validation  that email is unique in admin table

       // dd($request);
        $validator = Validator($request->all(),[
            'role_id'=>'required|integer|exists:roles,id',    //role id come from create screen 
           'name'=>'required|string|min:3|max:40',
           'email'=>'required|email|string|unique:admins,email'
        ]);
        if(! $validator->fails()){
            $roles =Role::findById($request->get('role_id'),'admin');  //should br return to guard admin
            $admin = new Admin();
            $admin->name=$request->get('name');
            $admin->email = $request->get('email');
            $admin->password= Hash::make(12345);
            $isSaved =$admin->save();
            if($isSaved){
                //give this admin role
                $admin->assignRole($roles);
            }
            return response()->json(['message'=>$isSaved ? 'Admin saved successfully ': 'Failed to save Admin'],$isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        }else{
            return response()->json(['message'=>$validator->getMessageBag()->first()],Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //How to know if it id empty or not 
        // $assignedRoles =$admin->getRoleNames();
        // $roles = Role::where('guard_name')->get();
        // //permission via role 
        // // $role =$admin->getRolesNames()[0]; // this func get return  the role for this admin //empty
        // $role= $assignedRoles->count()> 0 ? $assignedRoles[0] : null ; 
        // //اعطاء الرول اجباري  // we can't create admin without role so we don't need this senario
        // return response()->view('cms.admins.edit', ['admin'=>$admin ,'roles'=>$roles ,'assignedRole'=>$role]);

        $assignedRole = $admin->getRoleNames()[0];
        $roles = Role::where('guard_name','admin')->get();
        return response()->view('cms.admins.edit',
        ['admin' => $admin, 'roles' => $roles, 'assignedRole' => $assignedRole]);



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        //
        $validator = validator($request->all(),[
            'role_id' => 'required|integer|exists:roles,id',  
            'name' => 'required|string|min:3|max:40',
            'email' => 'required|email|string|unique:admins,email,'.$admin->id,
            //اذا اجيت اعدل و حطيت نفس الايميل القديم عادي مستثنى
            // اذا هذا الايميل الي دخل على الداتا بيز في التعديل هو نفس الايميل صاحب ال اي دي الي بحاول يعدل فيتم استنثاء حالة اليونيك ايميل 
        ]);
        if (!$validator->fails()) {
            $roles = Role::findById($request->get('role_id'), 'admin');  //should br return to guard admin
            $admin->name = $request->get('name');
            $admin->email = $request->get('email');
            $isSaved = $admin->save();
            if ($isSaved) {
                // if($admin->hasRole($roles)){
                //    $admin->assignRole($roles);  //give this admin role
                //    يعني انا عملت ابديت على نفس الرول ليش اعمل اسين تاني ؟ هي نفسها
                // بمنع اعادة اعطائها ثاني 
                // في حال بدي استبدل و بدي امحي القديم واحظ الجديد 
                $admin->syncRoles($roles); // Remove all current roles and set the given ones.
                
            }
            return response()->json(['message' => $isSaved ? 'Admin saved successfully ' : 'Failed to save Admin'], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json(['message' => $validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
         $isDeleted= $admin->delete();
        if ($isDeleted) {
            return response()->json(['title' => 'Deleted successfully', 'icon' => 'success']); //key>>string""  value>>int object double boolean array of any type or array of json  //begin with objects or array
        } else {
            return response()->json(['title' => 'Deleted failed', 'icon' => 'danger']);
        }
    }
}
