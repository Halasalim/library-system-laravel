<?php

namespace App\Http\Controllers;

use App\Models\Country;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;
use Symfony\Component\HttpFoundation\Response;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 
        $data = Country::all();
        return response()->view('cms.countries.index',['countries'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return response()->view('cms.countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation >> java script >> data >> array of rules 
        $validator = Validator($request->all(), [
            'name'=>'required|string|min:3|max:45',
        ]);
        
        if( !$validator ->fails()){ //is not failed
           //success    create 201  failed 400 Bad 
              $country = new Country(); //create country
              $country->name =$request->get('name');
              $isSaved =$country->save();
              return response()->json(['message'=> $isSaved ? 'Country created successfully' : 'Failed to create country'], 
              $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);

        }else{
            //Todo : falied  error message 400 (Bad) / 500
            //Validation error   //Bag error > first error 
            return response()->json(['message'=>$validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        //
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {
        //
        return response()->view('cms.countries.edit',['country' =>$country]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country)
    {
        //
        $validator = Validator($request-> all() ,[
            'name'=>'required|string|min:3|max:45'
        ]);

               if(!$validator -> fails()){
                   //TODO SUCCESS
                 
                   $country->name =$request->get('name');
                   $isSaved =$country->save();
                   return response()->json(['message'=> $isSaved ? 'Country updated successfully' : 'Failed to updated  country'], 
                   $isSaved ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST);
     
               }else{
                   
            return response()->json(['message'=>$validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);

               }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        //
        $isDeleted = $country->delete();
        return response()->json([
            'icon'=> $isDeleted ? 'success' :' danger',
            'title'=> $isDeleted ? 'Deleded successfully' : 'Failed to delete'],
         $isDeleted ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST);
    }
}
