<?php

namespace App\Providers;

// use App\Models\Category;
// use App\Policies\CategoryPolicy;

use Carbon\Carbon;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */


    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        // Category::class =>CategoryPolicy::class,
        //   if we named the policy with diferent name 
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
        Passport::routes();
        //create group of routes الخاصة بالمكتبة مسؤولة عن عملية توليد توكينز ياستخدم جرانت كلينت 
        // عنا ريكوست خاص اسمه POST      | oauth/token                                                   | passport.token
        //   | Laravel\Passport\Http\Controllers\AccessTokenController@issueToke
        //carbon مكتبة الوقت و التاريخ 
        //Passport Grant client 
        Passport::tokensExpireIn(Carbon::now()->addDays(15));
        //Personal Access client 
        Passport::personalAccessTokensExpireIn(Carbon::now()->addDays(15));
    }
}
