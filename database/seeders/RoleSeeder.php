<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //we  have 3 roles > 
        //1- super-admin he can controll all of the system
        //2-HR > ADMIN , USER 
        //3-content managment > controll the content categories and other
        //create > elqouent
     Role::create([
           'name'=>'Super-Admin',
           'guard_name'=>'admin',
     ]);
        Role::create([
            'name' => 'HR-Admin',
            'guard_name' => 'admin',
        ]);
        Role::create([
            'name' => 'Content-Management-Admin',
            'guard_name' => 'admin',
        ]);
    }
}
