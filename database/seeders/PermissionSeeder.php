<?php

namespace Database\Seeders;

use App\Http\Controllers\PermissionController;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //==================Admin Permissions =========================== //
        // Permission::create(['name'=>'Create-' ,'guard_name'=>'admin']);
        // Permission::create(['name' => 'Read-', 'guard_name' => 'admin']);
        // Permission::create(['name' => 'Update-', 'guard_name' => 'admin']);
        // Permission::create(['name' => 'Delete-', 'guard_name' => 'admin']);


        Permission::create(['name'=>'Create-Role' ,'guard_name'=>'admin']);
        Permission::create(['name' => 'Read-Roles', 'guard_name' => 'admin']);
        Permission::create(['name' => 'Update-Role', 'guard_name' => 'admin']);
        Permission::create(['name' => 'Delete-Role', 'guard_name' => 'admin']);


        Permission::create(['name' => 'CreatePermission', 'guard_name' => 'admin']);
        Permission::create(['name' => 'Read-Permissions', 'guard_name' => 'admin']);
        Permission::create(['name' => 'UpdatePermission', 'guard_name' => 'admin']);
        Permission::create(['name' => 'DeletePermission', 'guard_name' => 'admin']);

        Permission::create(['name'=>'Create-Admin' ,'guard_name'=>'admin']);
        Permission::create(['name' => 'Read-Admins', 'guard_name' => 'admin']);
        Permission::create(['name' => 'Update-Admin', 'guard_name' => 'admin']);
        Permission::create(['name' => 'Delete-Admin', 'guard_name' => 'admin']);

        Permission::create(['name'=>'Create-Category' ,'guard_name'=>'admin']);
        Permission::create(['name' => 'Read-Categories', 'guard_name' => 'admin']);
        Permission::create(['name' => 'Update-Category', 'guard_name' => 'admin']);
        Permission::create(['name' => 'Delete-Category', 'guard_name' => 'admin']);

        Permission::create(['name' => 'Create-Country', 'guard_name' => 'admin']);
        Permission::create(['name' => 'Read-Countries', 'guard_name' => 'admin']);
        Permission::create(['name' => 'Update-Country', 'guard_name' => 'admin']);
        Permission::create(['name' => 'Delete-Country', 'guard_name' => 'admin']);


        Permission::create(['name'=>'Create-Book' ,'guard_name'=>'admin']);
        Permission::create(['name' => 'Read-Books', 'guard_name' => 'admin']);
        Permission::create(['name' => 'Update-Book', 'guard_name' => 'admin']);
        Permission::create(['name' => 'Delete-Book', 'guard_name' => 'admin']);




        //==================User  Permissions =========================== //
        // Permission::create(['name' => 'Create-', 'guard_name' => 'user']);
        // Permission::create(['name' => 'Read-', 'guard_name' => 'user']);
        // Permission::create(['name' => 'Update-', 'guard_name' => 'user']);
        // Permission::create(['name' => 'Delete-', 'guard_name' => 'user']);

        Permission::create(['name' => 'Read-Categories', 'guard_name' => 'user']);
        Permission::create(['name' => 'Read-Books', 'guard_name' => 'user']);
    }
}
