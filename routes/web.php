<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Authcontroller;
use App\Http\Controllers\BookController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\RolePermissionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserPermissionController;
use App\Http\Middleware\checkAge;
use App\Mail\WelComeEmail;
use App\Models\Category;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */
//admin + user (web) both > login + logout 
//admin >> categories + countries 
//user > books 
//we need to add guard as parameter , dynamic attribute 

Route::prefix('cms/')->middleware('guest:admin,user')->group(function(){
  //  Route::view('login','cms.auth.login')->name('cms.login');
  Route::get('{guard}/login',[Authcontroller::class,'showlogin'])->name('cms.login');
  Route::post('login',[Authcontroller::class,'login']);
});

Route::prefix('cms/admin')->middleware('auth:admin')->group(function (){
  /*Note: route resbonsible to show >> index */
  Route::resource('admins', AdminController::class);
  Route::resource('roles', RoleController::class);
  Route::resource('permissions', PermissionController::class);
  
  Route::resource('permissions/role', RolePermissionController::class);
  Route::resource('permissions/user-permissions', UserPermissionController::class);
  Route::resource('users', UserController::class);
  

});

Route::prefix('cms/admin')->middleware('auth:admin,user')->group(function () {
  Route::view('/', 'cms.empty');
  Route::resource('books', BookController::class);
  Route::resource('categories', CategoryController::class);
  Route::resource('countries', CountryController::class);
  Route::get('change-password', [Authcontroller::class, 'showChangePassword'])->name('cms.changePassword');
  Route::post('change-password', [Authcontroller::class, 'changePassword']);
  Route::get('logout', [Authcontroller::class, 'logout'])->name('cms.logout');
});


Route::prefix('mail')->group(function(){
      Route::get('welcome',function(){
        return new WelComeEmail(); //markdown blade 
      });
});




//First way >> installation method of middleware 
// Route::get('news',function(){
//    return 'welcome to news page ';
// })->middleware('age:22',);


//second way 
// Route::get('news',function(){
  
//   /// return 'welcome to news page ';
//    return response()->json(['status'=>'success','active'=>true]);
//  })->middleware(checkAge::class);

 //third way 
//  Route::middleware('age:22',)->get('news',function(){
//     return 'welcome to news page ';
//  });

//forth way 
// Route::middleware('age:22',)->group(function(){
//   Route::get('news',function(){
//    return 'welcome to news page';
//      })->withoutMiddleware('age');
// });




