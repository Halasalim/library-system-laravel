<?php

use App\Http\Controllers\API\ApiAuthController;
use App\Http\Controllers\API\BookController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


// Route::get('laravel-9', function(){
//    return response()->json(['message'=>'welcome to laravel-9 APi']);
// });

Route::prefix('auth')->group(function(){
   Route::post('login',[ApiAuthController::class, 'login']);
});

//logout  //user
//هاي العمليات الي جوا القروب ما بنفذها حدا الا اذا كان معه توكين مش ريفوك و مش اكسبيرد
Route::prefix('auth')->middleware('auth:api')->group(function () {
  Route::get('logout',[ApiAuthController::class, 'logout']);
  //هان لما نفذت الريكسوت وقفني الميدلوير و رجعني على اللوجين
  //عشان يعرف اني جاي من api  في عدة طرق :
});
Route::middleware('auth:api')->group(function () {
  Route::apiResource('books', BookController::class);

});