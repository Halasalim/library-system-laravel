function confirmDestroy(url ,id , ref){
	// console.log('category:'+id);
	Swal.fire({
   title: 'Are you sure?',
  text: "You won't be able to revert this!", //subtitel
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
  if (result.isConfirmed) {
    deleteItem(url,id ,ref);
  }
})
	 }

	function deleteItem(url ,id , ref){
   	axios.delete(url+'/' +id)
  .then(function (response) {
    // handle success code 2xx
    console.log(response);
	showMessage(response.data);
	ref.closest('tr').remove();
  })
  .catch(function (error) {
    // handle error 4xx 5xx
    console.log(error);       
	showMessage(error.response.data);
  });
  
	}

	function showMessage(data){
		 
   Swal.fire({
  //position: 'top-end',
  icon: data.icon,
  title: data.title,
  showConfirmButton: false,
  timer: 1500
})
	}


function store(url, data) {
    axios
        .post(url, data)
        .then(function (response) {
            // handle success code 2xx
            console.log(response);
            //showMessage(response.data);
            toastr.success(response.data.message);
            document.getElementById("create-form").reset();
        })
        .catch(function (error) {
            // handle error 4xx 5xx
            console.log(error);
            //showMessage(error.response.data);
            toastr.error(error.response.data.message);
        });
}

function update(url , data , redirectRoute){
   	
  axios.put(url  , data)
 .then(function (response) {
 // handle success code 2xx
  console.log(response);
 //showMessage(response.data);
//toastr.success(response.data.message);
if(redirectRoute != undefined){
  window.location.href= redirectRoute; //اعادة توجيهه
}else{
   toastr.success(response.data.message);
}


})
.catch(function (error) {
// handle error 4xx 5xx
console.log(error);       
//showMessage(error.response.data);
toastr.error(error.response.data.message)
});

}