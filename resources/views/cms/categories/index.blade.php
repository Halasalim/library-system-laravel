@extends('cms.parent')

@section('title','Categories')
@section('page-large-name','Categories')
@section('page-small-name','Index-show')

@section('styles')

@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
		<div class="container-fluid">
		  <div class="row">
			<div class="col-md-12">
			  <div class="card">
				<div class="card-header">
				  <h3 class="card-title">Bordered Table</h3>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
				  <table class="table table-bordered table-striped table-hover">
					<thead>
					  <tr>
						<th style="width: 10px">#</th>
						<th>Name</th>
						<th>Books</th>
						<th>Visible</th>
						<th>Created At</th>
						<th>Updated At</th>
						<th>Settings</th>
					  </tr>
					</thead>
					<tbody>
						@foreach ($categories as $data)
						<tr>
							<td>{{$data->id}}</td>
							<td>{{$data->name}}</td>
							<td>
								<span class="badge bg-info">({{$data->books_count}}) Book/s</span>
							</td>
							

							
							<td>
								<span class="badge @if ($data->is_visible) bg-success
									
								@else  bg-danger @endif">	{{$data->visibility_status}}</span>
							</td>
							<td>{{$data->created_at->format('y-m-d H:ma')}}</td>
							<td>{{$data->updated_at->format('y-m-d H:ma')}}</td>
							<td>
								<div class="btn-group">
										<!--Edit  Button-->
									<a href="{{route('categories.edit',$data->id)}}" class="btn btn-info">
									  <i class="fas fa-edit"></i>  
									</a>
	<!--Java Script way-->
									<a onclick="deleteCountry({{$data->id}}, this )" class="btn btn-danger">
										<i class="fas fa-trash"></i>
									</a>
									<!--WE Can't sent a check request  without using Form , if we sent the method as get then will be cause a conflict 
									because their is the same route as the delete (show)   so we put the type post to avoid conflict and gose direct to delete 
								-->
								<!--Delete Button traditional way-->

									{{-- <form method="POST" action="{{route('categories.destroy',$data->id)}}"> 
										@csrf 
										@method('DELETE')
									<button type="submit" class="btn btn-danger">
									  <i class="fas fa-trash"></i>
									</button>
								</form> --}}
							
							
								  </div>
							</td>
							
						  </tr>
						@endforeach
						<!--For Knowledge-->
						{{--@if ($data->visible)
						<td>True</td>
					     @else
					     <td>False</td>
					     @endif--}}
					
					</tbody>
				  </table>
				</div>
				<!-- /.card-body -->
		
			  </div>
			  <!-- /.card -->
  
			
			</div>
			<!-- /.col -->
		  </div> 
		  <!-- /.row -->
		</div><!-- /.container-fluid -->
	  </section>
	  <!-- /.content -->
@endsection


@section('scripts')
 <script>
	 

	 function deleteCountry(id ,ref){
		confirmDestroy('/cms/admin/categories',id ,ref)
	 }
	 </script>


@endsection

{{-- Swal.fire(
	'Deleted!',
	'Your file has been deleted.',
	'success'
  ) --}}