@extends('cms.parent')

@section('title','DEMO')
@section('page-large-name','Library System')
@section('page-small-name','categories')

@section('styles')

@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
		<div class="container-fluid">
		  <div class="row">
			<!-- left column -->
			<div class="col-md-12">
			  <!-- general form elements -->
			  <div class="card card-primary">
				<div class="card-header">
				  <h3 class="card-title">Create Category</h3>
				</div>


			

				<!-- /.card-header -->
				<!-- form start -->
				<form method="POST" action="{{route('categories.store')}}" >
					@csrf

					<!--we need to make it appear when their is an error  , $errors = we make validation auto request validator return array (errors)
					result of failer >> errors of validation rules  the next step we need to show the errors (li) >>loop >>
					value old >> return old data 
					-->
				      
				  <div class="card-body">
					@if ($errors->any())
					<div class="alert alert-danger alert-dismissible">
					   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					   <h5><i class="icon fas fa-ban"></i> Alert!</h5>
					   @foreach ($errors->all() as $error)
							 <li>{{$error}}</li>
					   @endforeach
				   
					 </div>
					 
					 @endif
					 @if (session()->has('message')) 
					 <div class="alert alert-{{session()->get('type')}} alert-dismissible">
					   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					   <h5><i class="icon fas fa-check"></i> Success !</h5>
								{{session()->get('message')}}
					 </div>
					 @endif
					<div class="form-group">
					  <label for="name">Name</label>
					  <input type="text" class="form-control" value="{{old('name')}}"  name="name" id="name" placeholder="Enter name">
					</div>
					<div class="form-group">
					  <label for="Description">Description</label>
					  <input type="text" class="form-control"   value="{{old('Description')}}" name="Description" id="Description" placeholder="Description">
					</div>
				
					<div class="form-group">
						<div class="custom-control custom-switch">
						  <input type="checkbox" class="custom-control-input" name="visible" id="visible" checked >
						  <label class="custom-control-label" for="visible">visible</label>
						</div>
					  </div>
				
				  </div>
				  <!-- /.card-body -->
  
				  <div class="card-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				  </div>
				</form>
			  </div>
			  <!-- /.card -->
  
		
  
			</div>
			<!--/.col (left) -->
		
		  </div>
		  <!-- /.row -->
		</div><!-- /.container-fluid -->
	  </section>
	  <!-- /.content -->
@endsection
 

@section('scripts')
@endsection