@extends('cms.parent')

@section('title','Permissions')
@section('page-large-name','Permissions')
@section('page-small-name','Index')

@section('styles')

@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
		<div class="container-fluid">
		  <div class="row">
			<div class="col-md-12">
			  <div class="card">
				<div class="card-header">
				  <h3 class="card-title">Bordered Table</h3>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
				  <table class="table table-bordered table-striped table-hover">
					<thead>
					  <tr>
						<th style="width: 10px">#</th>
						<th>Name</th>
						<th>Guard</th>
						<th>Created At</th>
						<th>Updated At</th>
						<th>Settings</th>
					  </tr>
					</thead>
					<tbody>
						@foreach ($permissions as $permission)
						<tr>
							<td>{{$permission->id}}</td>
							<td>{{$permission->name}}</td>
						
							<td>
								<span class="badge bg-info">{{$permission->guard_name}}</span>
							</td>
							
							
							<td>{{$permission->created_at->format('y-m-d H:ma')}}</td>
							<td>{{$permission->updated_at->format('y-m-d H:ma')}}</td>
							<td>
								<div class="btn-group">
										<!--Edit  Button-->
									<a href="{{route('permissions.edit',$permission->id)}}" class="btn btn-info">
									  <i class="fas fa-edit"></i>  
									</a>
	<!--Java Script way-->
									<a onclick="deletepermission({{$permission->id}}, this )" class="btn btn-danger">
										<i class="fas fa-trash"></i>
									</a>
								
							
							
								  </div>
							</td>
							
						  </tr>
						@endforeach
						<!--For Knowledge-->
						{{--@if ($data->visible)
						<td>True</td>
					     @else
					     <td>False</td>
					     @endif--}}
					
					</tbody>
				  </table>
				</div>
				<!-- /.card-body -->
		
			  </div>
			  <!-- /.card -->
  
			
			</div>
			<!-- /.col -->
		  </div> 
		  <!-- /.row -->
		</div><!-- /.container-fluid -->
	  </section>
	  <!-- /.content -->
@endsection


@section('scripts')
 <script>
	 
	 function deletepermission(id ,ref){
		confirmDestroy('/cms/admin/permissions',id ,ref)
	 }
	 </script>

@endsection

{{-- Swal.fire(
	'Deleted!',
	'Your file has been deleted.',
	'success'
  ) --}}