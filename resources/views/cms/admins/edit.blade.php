@extends('cms.parent')

@section('title','DEMO')
@section('page-large-name','Library System')
@section('page-small-name','Admin')

@section('styles')
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('cms/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('cms/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
		<div class="container-fluid">
		  <div class="row">
			<!-- left column -->
			<div class="col-md-12">
			  <!-- general form elements -->
			  <div class="card card-primary">
				<div class="card-header">
				  <h3 class="card-title">Edit  Admin</h3>
				</div>


			

				<!-- /.card-header -->
				<!-- form start -->
				<form id="create-form">
					@csrf

					<!--we need to make it appear when their is an error  , $errors = we make validation auto request validator return array (errors)
					result of failer >> errors of validation rules  the next step we need to show the errors (li) >>loop >>
					value old >> return old data 
					-->
				      
				  <div class="card-body">
					<div class="form-group">
								<label>Role</label>
								<select class="form-control roles" id="role_id" style="width: 100%;">
									{{-- <option selected="selected">Alabama</option> --}}
									@foreach ($roles as $role )
									<option value="{{$role->id}}" @if($assignedRole==$role->name) selected @endif>{{$role->name}}</option>
									@endforeach
							              {{-- اذا كان النيم المعطى هو عبارة عن النيم تبع الرول اظهره  --}}
							
								</select>
							</div>
					<div class="form-group">
					  <label for="name">Name</label>
					  <input type="text" class="form-control" name="name" value="{{$admin->name}}"
					   id="name" placeholder="Enter name">
					</div>
					<div class="form-group">
					  <label for="email">Email</label>
					  <input type="email" class="form-control"   name="email" value="{{$admin->email}}
					  "id="email" placeholder="Enter Email">
					</div>
				
				  </div>
				  <!-- /.card-body -->
  
				  <div class="card-footer">
					<button type="button" onclick="performUpdate('{{$admin->id}}')" class="btn btn-primary">Submit</button>
				  </div>
				</form>
			  </div>
			  <!-- /.card -->
  
		
  
			</div>
			<!--/.col (left) -->
		
		  </div>
		  <!-- /.row -->
		</div><!-- /.container-fluid -->
	  </section>
	  <!-- /.content -->
@endsection
 

@section('scripts')
<!-- Select2 -->
<script src="{{asset('cms/plugins/select2/js/select2.full.min.js')}}"></script>

<script>
	$('.roles').select2({
	theme: 'bootstrap4' //Bootstrap
	});
	function performUpdate(id){
		
		let data ={
			role_id:document.getElementById('role_id').value,
           name :document.getElementById('name').value,
		   email:document.getElementById('email').value,
		}
		update('/cms/admin/admins/'+id ,data, '/cms/admin/admins'); //ref
	}
</script>

@endsection