@extends('cms.parent')

@section('title','DEMO')
@section('page-large-name','Library System')
@section('page-small-name','Roles')

@section('styles')
 <!-- Select2 -->
 <link rel="stylesheet" href="{{asset('cms/plugins/select2/css/select2.min.css')}}">
 <link rel="stylesheet" href="{{asset('cms/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
		<div class="container-fluid">
		  <div class="row">
			<!-- left column -->
			<div class="col-md-12">
			  <!-- general form elements -->
			  <div class="card card-primary">
				<div class="card-header">
				  <h3 class="card-title">Create Role</h3>
				</div>


			

				<!-- /.card-header -->
				<!-- form start -->
				<form method="POST" action="{{route('roles.store')}}" >
					@csrf

					<!--we need to make it appear when their is an error  , $errors = we make validation auto request validator return array (errors)
					result of failer >> errors of validation rules  the next step we need to show the errors (li) >>loop >>
					value old >> return old data 
					-->
				      
				  <div class="card-body">
					<div class="form-group">
						<label>Guard</label>
						<select class="form-control guard" id ="guard" style="width: 100%;">
						  {{-- <option selected="selected">Alabama</option> --}}
						  
						  <option  value="admin">Admin</option>
						  <option value="user">User</option>

						  
						
						</select>
					  </div>
					<div class="form-group">
					  <label for="name">Name</label>
					  <input type="text" class="form-control"  name="name" id="name" placeholder="Enter name">
					</div>

				  </div>
				  <!-- /.card-body -->
  
				  <div class="card-footer">
					<button type="button" onclick="performStore()" class="btn btn-primary">Submit</button>
				  </div>
				</form>
			  </div>
			  <!-- /.card -->
  
		
  
			</div>
			<!--/.col (left) -->
		
		  </div>
		  <!-- /.row -->
		</div><!-- /.container-fluid -->
	  </section>
	  <!-- /.content -->
@endsection
 

@section('scripts')
<!-- Select2 -->
<script src="{{asset('cms/plugins/select2/js/select2.full.min.js')}}"></script>

<script>
 $('.guard').select2({
      theme: 'bootstrap4' //Bootstrap 
    });


	//Book store 
	function performStore(){
     let data ={
		 name :document.getElementById('name').value,
		  guard :document.getElementById('guard').value,
		 
	 }

	 store('/cms/admin/roles',data);
	}

</script>

@endsection