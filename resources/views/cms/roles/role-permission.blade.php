@extends('cms.parent')

@section('title','Roles-Permissions')
@section('page-large-name','Roles-Permission')
@section('page-small-name','Index')

@section('styles')
<link rel="stylesheet" href="{{asset('cms/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
		<div class="container-fluid">
		  <div class="row">
			<div class="col-md-12">
			  <div class="card">
				<div class="card-header">
				  <h3 class="card-title">{{$role->name}} Permission </h3>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
				  <table class="table table-bordered table-striped table-hover">
					<thead>
					  <tr>
						<th style="width: 10px">#</th>
						<th>Permission Name</th>
						<th>Permission Guard</th>
						<th>Assigned</th>
					  </tr>
					</thead>
					<tbody>
						@foreach ($permissions as $permission)
						<tr>
							<td>{{$permission->id}}</td>
							<td>{{$permission->name}}</td>
						
							<td>
								<!--We need to send number of permissions-->
								<span class="badge bg-info">{{$permission->guard_name}}</span>
							</td>	
							<td>
								
							<div class="icheck-success d-inline">
								<input type="checkbox" 
								 onclick="assinedPermission('{{$role->id}}','{{$permission->id}}')"
								@if($permission->assigned)
								checked 
								@endif
								id="permission_{{$permission->id}}">
								<label for="permission_{{$permission->id}}">
								</label>
							</div>
							</td>
							
						  </tr>
						@endforeach
						<!--For Knowledge-->
						{{--@if ($data->visible)
						<td>True</td>
					     @else
					     <td>False</td>
					     @endif--}}
					
					</tbody>
				  </table>
				</div>
				<!-- /.card-body -->
		
			  </div>
			  <!-- /.card -->
  
			
			</div>
			<!-- /.col -->
		  </div> 
		  <!-- /.row -->
		</div><!-- /.container-fluid -->
	  </section>
	  <!-- /.content -->
@endsection


@section('scripts')
 <script>
	 
	//we need to add permissions for role 
	//every check on permission will be add 
	function assinedPermission(roleId , permissionId){
	axios.post('/cms/admin/permissions/role' , {
		role_id :roleId,
		permission_id :permissionId
	})
	.then(function (response) {
	// handle success code 2xx
	console.log(response);
	toastr.success(response.data.message)
	})
	.catch(function (error) {
	// handle error 4xx 5xx
	console.log(error);
	toastr.error(error.response.data.message)
	});
	
	}
	 </script>

@endsection

{{-- Swal.fire(
	'Deleted!',
	'Your file has been deleted.',
	'success'
  ) --}}