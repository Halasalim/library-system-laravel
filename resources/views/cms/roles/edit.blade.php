@extends('cms.parent')

@section('title','DEMO')
@section('page-large-name','Library System')
@section('page-small-name','Books')

@section('styles')

@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
		<div class="container-fluid">
		  <div class="row">
			<!-- left column -->
			<div class="col-md-12">
			  <!-- general form elements -->
			  <div class="card card-primary">
				<div class="card-header">
				  <h3 class="card-title">Update Book</h3>
				</div>


			

				<!-- /.card-header -->
				<!-- form start -->
				<form method="POST" >
					@csrf
                  
					<!--we need to make it appear when their is an error  , $errors = we make validation auto request validator return array (errors)
					result of failer >> errors of validation rules  the next step we need to show the errors (li) >>loop >>
					value old >> return old data 
					-->
				      
				  <div class="card-body">
					<div class="form-group">

                     
					<div class="form-group">
					  <label for="name">Name</label>
					  <input type="text" class="form-control"  name="name" id="name" placeholder="Enter name">
					</div>

					<div class="form-group">
					  <label for="year">Year</label>
					  <input type="number" class="form-control"   id="year" placeholder="year">
					</div>

					<div class="form-group">
						<label>Language</label>
						<select class="form-control languages" id= "language" style="width: 100%;">
						  <option  value="en">English</option>
						  <option  value="ar">Arabic</option>
						  
						
						</select>
					  </div>


					  <div class="form-group">
						<label for="quantity">Quantity</label>
						<input type="number" class="form-control"   id="quantity" placeholder="quantity">
					  </div>
				
					<div class="form-group">
						<div class="custom-control custom-switch">
						  <input type="checkbox" class="custom-control-input" name="visible" id="visible" checked >
						  <label class="custom-control-label" for="visible">visible</label>
						</div>
					  </div>
				
				  </div>
				  <!-- /.card-body -->
  
				  <div class="card-footer">
					<button type="button" onclick="updateBook({{$book->id}})" class="btn btn-primary">Submit</button>
				  </div>

					  
				</form>
			  </div>
			  <!-- /.card -->
  
		
  
			</div>
			<!--/.col (left) -->
		
		  </div>
		  <!-- /.row -->
		</div><!-- /.container-fluid -->
	  </section>
	  <!-- /.content -->
@endsection
 

@section('scripts')


<script>
	   function updateBook(id){
				 let data ={
		        name :document.getElementById('name').value,
	        	 year :document.getElementById('year').value,
	        	 quantity :document.getElementById('quantity').value,
		       //  category_id :document.getElementById('category_id').value,
		 language :document.getElementById('language').value,
	        	 visible :document.getElementById('visible').checked,

				 }
				 update('/cms/admin/books/'+ id,data,'/cms/admin/books')
			 }
</script>
 

@endsection