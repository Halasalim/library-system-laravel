@extends('cms.parent')

@section('title','Books')
@section('page-large-name','Books')
@section('page-small-name','Index-show')

@section('styles')

@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
		<div class="container-fluid">
		  <div class="row">
			<div class="col-md-12">
			  <div class="card">
				<div class="card-header">
				  <h3 class="card-title">Bordered Table</h3>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
				  <table class="table table-bordered table-striped table-hover">
					<thead>
					  <tr>
						<th style="width: 10px">#</th>
						<th>Image</th>
						<th>Name</th>
						<th>Category</th>
						<th>Year</th>
						<th>language</th>
						<th>Quantity</th>
						<th>Visible</th>
						<th>Created At</th>
						<th>Updated At</th>
						<th>Settings</th>
					  </tr>
					</thead>
					<tbody>
						@foreach ($books as $book)
						<tr>
							<td>{{$book->id}}</td>
							<td>
								<img class="img-circle img-bordered-sm"  height="60" width="60"
								src="{{Storage::url('books/'.$book->image)}}" alt="user image">
							</td>
							<td>{{$book->name}}</td>
							<td>{{$book->category->name}}</td>
							<td>
								<span class="badge bg-info">({{$book->year}})</span>
							</td>
							<td>
								<span class="badge bg-success">	{{$book->language_name}}</span>
							</td>
							<td>
								<span class="badge bg-primary">({{$book->quantity}}) book/s</span>
							</td>
							<td>
								<span class="badge bg-info">({{$book->visibility_status}})</span>
							</td>
							
							<td>{{$book->created_at->format('y-m-d H:ma')}}</td>
							<td>{{$book->updated_at->format('y-m-d H:ma')}}</td>
							<td>
								<div class="btn-group">
										<!--Edit  Button-->
									<a href="{{route('books.edit',$book->id)}}" class="btn btn-info">
									  <i class="fas fa-edit"></i>  
									</a>
	<!--Java Script way-->             
	                                @if($book->trashed())
									<a onclick="deleteBook({{$book->id}}, this )" class="btn btn-warning">
										<i class="fas fa-trash-restore"></i>
										{{-- <i class="fas fa-trash-restore"></i> --}}
									</a>
									@else
                                         <a onclick="deleteBook({{$book->id}}, this )" class="btn btn-danger">
												<i class="fas fa-trash"></i>
											</a>
									@endif
								
									<!--WE Can't sent a check request  without using Form , if we sent the method as get then will be cause a conflict 
									because their is the same route as the delete (show)   so we put the type post to avoid conflict and gose direct to delete 
								-->
								<!--Delete Button traditional way-->

									{{-- <form method="POST" action="{{route('categories.destroy',$data->id)}}"> 
										@csrf 
										@method('DELETE')
									<button type="submit" class="btn btn-danger">
									  <i class="fas fa-trash"></i>
									</button>
								</form> --}}
							
							
								  </div>
							</td>
							
						  </tr>
						@endforeach
						<!--For Knowledge-->
						{{--@if ($data->visible)
						<td>True</td>
					     @else
					     <td>False</td>
					     @endif--}}
					
					</tbody>
				  </table>
				</div>
				<!-- /.card-body -->
		
			  </div>
			  <!-- /.card -->
  
			
			</div>
			<!-- /.col -->
		  </div> 
		  <!-- /.row -->
		</div><!-- /.container-fluid -->
	  </section>
	  <!-- /.content -->
@endsection


@section('scripts')
 <script>
	 
	 function deleteBook(id ,ref){
		confirmDestroy('/cms/admin/books',id ,ref)
	 }
	 </script>

@endsection

{{-- Swal.fire(
	'Deleted!',
	'Your file has been deleted.',
	'success'
  ) --}}