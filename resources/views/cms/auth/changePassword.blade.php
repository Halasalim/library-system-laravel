@extends('cms.parent')

@section('title','DEMO')
@section('page-large-name','Library System')
@section('page-small-name','categories')

@section('styles')

@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
		<div class="container-fluid">
		  <div class="row">
			<!-- left column -->
			<div class="col-md-12">
			  <!-- general form elements -->
			  <div class="card card-primary">
				<div class="card-header">
				  <h3 class="card-title">Change Password</h3>
				</div>
				<!-- /.card-header -->
				<!-- form start -->
				<form id="change-password-form">
					@csrf

					<!--we need to make it appear when their is an error  , $errors = we make validation auto request validator return array (errors)
					result of failer >> errors of validation rules  the next step we need to show the errors (li) >>loop >>
					value old >> return old data 
					-->
				      
				  <div class="card-body">
					<div class="form-group">
					  <label for="password">Current Password</label>
					  <input type="password" class="form-control"   id="password" placeholder="Enter Current Password">
					</div>

					<div class="form-group">
					  <label for="new_password">New Password</label>
					  <input type="password" class="form-control"   id="new_password" placeholder="Enter New Password">
					</div>

					<div class="form-group">
					  <label for="new_password_confirmation">Confirm New Password</label>
					  <input type="password" class="form-control"  id="new_password_confirmation" placeholder="Confirm password ">
					</div>

				  </div>
				  <!-- /.card-body -->
  
				  <div class="card-footer">
					<button type="button" onclick="changePassword()" class="btn btn-primary">Submit</button>
				  </div>
				</form>
			  </div>
			  <!-- /.card -->
  
		
  
			</div>
			<!--/.col (left) -->
		
		  </div>
		  <!-- /.row -->
		</div><!-- /.container-fluid -->
	  </section>
	  <!-- /.content -->
@endsection
 

@section('scripts')

<script>
  function changePassword(){
    axios.post('/cms/admin/change-password' , {
      password:document.getElementById('password').value,
      new_password:document.getElementById('new_password').value,
      new_password_confirmation:document.getElementById('new_password_confirmation').value,
  
    }).then(function (response) {
   // handle success code 2xx
      console.log(response);
      document.getElementById('change-password-form').reset();
      toastr.success(response.data.message)
})
.catch(function (error) {
 // handle error 4xx 5xx
 console.log(error);       
 toastr.error(error.response.data.message)
});
  }

</script>
@endsection