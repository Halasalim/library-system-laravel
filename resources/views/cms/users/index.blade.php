@extends('cms.parent')

@section('title','Users')
@section('page-large-name','Users')
@section('page-small-name','Show')

@section('styles')

@endsection

@section('content')
<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Bordered Table</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<table class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th style="width: 10px">#</th>
									<th>Name</th>
									<th>Email</th>
									<th>Permissions</th>
									<th>Created At</th>
									<th>Updated At</th>
									<th>Settings</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($users as $user)
								<tr>
									<td>{{$user->id}}</td>
									<td>{{$user->name}}</td>
									<td>
										<span class="badge bg-info">{{$user->email}}</span>
									</td>
									<td>
										<a href="{{route('user-permissions.show' , $user->id)}}" class="btn btn-block btn-warning btn-sm">
											({{$user->permissions_count}}) Permission/s
										</a>
									</td>
									<td>{{$user->created_at->format('y-m-d H:ma')}}</td>
									<td>{{$user->updated_at->format('y-m-d H:ma')}}</td>
									<td>
										<div class="btn-group">
											<!--Edit  Button-->
											<a href="{{route('users.edit',$user->id)}}" class="btn btn-info">
												<i class="fas fa-edit"></i>
											</a>
											<!--Java Script way-->
											<a onclick="performdelete({{$user->id}}, this )" class="btn btn-danger">
												<i class="fas fa-trash"></i>
											</a>
											<!--WE Can't sent a check request  without using Form , if we sent the method as get then will be cause a conflict 
									because their is the same route as the delete (show)   so we put the type post to avoid conflict and gose direct to delete 
								-->


											<!--Delete Button traditional way-->

											{{-- <form method="POST" action="{{route('categories.destroy',$user->id)}}">
											@csrf
											@method('DELETE')
											<button type="submit" class="btn btn-danger">
												<i class="fas fa-trash"></i>
											</button>
											</form> --}}


										</div>
									</td>

								</tr>
								@endforeach
								<!--For Knowledge-->
								{{--@if ($user->visible)
						<td>True</td>
					     @else
					     <td>False</td>
					     @endif--}}

							</tbody>
						</table>
					</div>
					<!-- /.card-body -->

				</div>
				<!-- /.card -->


			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection


@section('scripts')
<script>
	function performdelete(id ,ref){
		confirmDestroy('/cms/admin/users',id ,ref)
	 }
</script>


@endsection

{{-- Swal.fire(
	'Deleted!',
	'Your file has been deleted.',
	'success'
  ) --}}