@extends('cms.parent')

@section('title','DEMO')
@section('page-large-name','Library System')
@section('page-small-name','categories')

@section('styles')

@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
		<div class="container-fluid">
		  <div class="row">
			<!-- left column -->
			<div class="col-md-12">
			  <!-- general form elements -->
			  <div class="card card-primary">
				<div class="card-header">
				  <h3 class="card-title">Update Country</h3>
				</div>

				<!-- /.card-header -->
				<!-- form start -->
				<form method="POST"  >
					@csrf
                   
					<!--we need to make it appear when their is an error  , $errors = we make validation auto request validator return array (errors)
					result of failer >> errors of validation rules  the next step we need to show the errors (li) >>loop >>
					value old >> return old data 
					-->
				      
				  <div class="card-body">	
					<div class="form-group">
					  <label for="name">Name</label>
					  <input type="text" class="form-control"  value="{{$country->name}}" name="name" id="name" placeholder="Enter name">
					</div>
					
				
				  </div>
				  <!-- /.card-body -->
  
				  <div class="card-footer">
					<button type="button" onclick="updateItem({{$country->id}})" class="btn btn-primary">Submit</button>
				  </div>
				</form>
			  </div>
			  <!-- /.card -->
  
		
  
			</div>
			<!--/.col (left) -->
		
		  </div>
		  <!-- /.row -->
		</div><!-- /.container-fluid -->
	  </section>
	  <!-- /.content -->
@endsection
 

@section('scripts')

<script>
             function updateItem(id){
				 let data ={
					 name:document.getElementById('name').value
				 }
				 update('/cms/admin/countries/'+ id,data,'/cms/admin/countries')
			 }

	</script>

@endsection