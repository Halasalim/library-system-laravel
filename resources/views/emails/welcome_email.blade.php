@component('mail::message')
# Introduction test email

The body of your message.

@component('mail::button', ['url' => 'http://127.0.0.1:8000/cms/admin'])
OPEN CMS 
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
